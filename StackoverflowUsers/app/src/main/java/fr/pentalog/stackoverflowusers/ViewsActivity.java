package fr.pentalog.stackoverflowusers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ionut Stirban on 28/03/16.
 */
public class ViewsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.views_activity);
    }
}
