package fr.pentalog.stackoverflowusers.userslist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ionut Stirban on 27/03/16.
 */
public class User {

    @SerializedName("display_name")
    private String displayName;

    @SerializedName("profile_image")
    private String profileImage;

    private int age;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
