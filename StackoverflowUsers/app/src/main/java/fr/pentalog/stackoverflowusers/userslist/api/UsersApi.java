package fr.pentalog.stackoverflowusers.userslist.api;

import fr.pentalog.stackoverflowusers.userslist.model.UsersResponse;
import retrofit2.http.GET;

/**
 * Created by Ionut Stirban on 27/03/16.
 */
public interface UsersApi {
    @GET("users?site=stackoverflow")
    retrofit2.Call<UsersResponse> getUsers();
}
