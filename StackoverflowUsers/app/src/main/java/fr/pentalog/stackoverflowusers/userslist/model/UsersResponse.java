package fr.pentalog.stackoverflowusers.userslist.model;

import java.util.List;

/**
 * Created by Ionut Stirban on 27/03/16.
 */
public class UsersResponse {
    private List<User> items;

    public List<User> getItems() {
        return items;
    }

    public void setItems(List<User> items) {
        this.items = items;
    }
}
