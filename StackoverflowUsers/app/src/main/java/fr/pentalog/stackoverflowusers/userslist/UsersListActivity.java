package fr.pentalog.stackoverflowusers.userslist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import fr.pentalog.stackoverflowusers.R;
import fr.pentalog.stackoverflowusers.userslist.api.UsersApi;
import fr.pentalog.stackoverflowusers.userslist.model.User;
import fr.pentalog.stackoverflowusers.userslist.model.UsersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsersListActivity extends AppCompatActivity {
    private ListView lv_users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_list_activity);
        linkUI();
        getUsers();
    }

    private void linkUI() {
        lv_users = (ListView) findViewById(R.id.lv_users);
    }

    public void getUsers() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.stackexchange.com/2.2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UsersApi service = retrofit.create(UsersApi.class);
        service.getUsers()
                .enqueue(new Callback<UsersResponse>() {
                    @Override
                    public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {
                        if (response.isSuccessful()) {
                            showList(response.body().getItems());
                        }
                    }

                    @Override
                    public void onFailure(Call<UsersResponse> call, Throwable t) {

                    }
                });
    }

    private void showMessage() {

    }

    private void showList(List<User> users) {
        lv_users.setAdapter(new UsersAdapter(this, users));

    }
}
