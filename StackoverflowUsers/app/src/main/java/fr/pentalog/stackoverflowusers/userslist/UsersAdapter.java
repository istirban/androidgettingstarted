package fr.pentalog.stackoverflowusers.userslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import fr.pentalog.stackoverflowusers.R;
import fr.pentalog.stackoverflowusers.userslist.model.User;

/**
 * Created by Ionut Stirban on 27/03/16.
 */
class UsersAdapter extends BaseAdapter {
    private List<User> users;
    private Context context;
    private LayoutInflater layoutInflater;

    UsersAdapter(Context context, List<User> users) {
        this.users = users;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final UsersHolder usersHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.user_item, parent, false);
            usersHolder = new UsersHolder(convertView);
            convertView.setTag(usersHolder);
        } else {
            usersHolder = (UsersHolder) convertView.getTag();
        }

        final User user = users.get(position);
        usersHolder.txtUserName.setText(user.getDisplayName());
        usersHolder.txtAge.setText(String.valueOf(user.getAge()));
        Glide.with(context).load(user.getProfileImage()).into(usersHolder.ivUserProfileImage);

        return convertView;
    }

    private static class UsersHolder {
        ImageView ivUserProfileImage;
        TextView txtUserName;
        TextView txtAge;

        UsersHolder(View cell) {
            ivUserProfileImage = (ImageView) cell.findViewById(R.id.iv_user_profile_image);
            txtUserName = (TextView) cell.findViewById(R.id.txt_user_name);
            txtAge = (TextView) cell.findViewById(R.id.txt_age);
        }
    }
}
